#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


df_rest = pd.read_csv('ex1data1.txt', names=['population', 'income'])


# In[3]:


import plotly.express as px


# In[4]:


fig = px.scatter(df_rest, x='population', y='income', title='Restourant overview')


# In[5]:


fig.show()


# In[6]:


import numpy as np

def hypothesis(x: np.array, t0: float, t1: float):
    return t0 + x * t1


# In[7]:


def loss(x, t0, t1, real_y):
    return np.mean((hypothesis(x, t0, t1) - real_y)**2)


# In[8]:


loss(df_rest['population'], -2, 2.5, df_rest['income'])


# In[9]:


class RestourantCounter:
    def __init__(self, df: pd.DataFrame, t0=0.0, t1=0.0):
        self.population = df['population']
        self.income = df['income']
        self.t0 = t0
        self.t1 = t1
    
    def set_coeff_and_count_loss(self, t0: float, t1: float):
        self.set_coef(t0, t1)
        return self.count_loss()
        
    def set_coef(self,  t0: float, t1: float):
        self.t0 = t0
        self.t1 = t1
    
    def count_hipothesis(self):
        return self.t0 + self.population * self.t1
    
    def count_loss(self):
        return np.mean((self.count_hipothesis() - self.income)**2)
    
    def count_derivative_t0(self):
        return 2 / self.population.count() * (
            np.sum(self.count_hipothesis() - self.income)
        )    
    
    def count_derivative_t1(self):
        return 2 / self.population.count() * (
            np.sum((self.count_hipothesis() - self.income) * self.population)
        )
    
    def make_gradient_descent(self, alpha: float):
        next_t0 = self.t0 - self.count_derivative_t0() * alpha
        next_t1 = self.t1 - self.count_derivative_t1() * alpha
        
        return self.set_coeff_and_count_loss(next_t0, next_t1)


# In[10]:


MAX_ITER = 10000
alpha = 0.001
epsilon = 0.00001

counter = RestourantCounter(df_rest)
prev_loss = counter.count_loss()
i = 0
loss_steps = [prev_loss]

while i < MAX_ITER:
    loss = counter.make_gradient_descent(alpha)
    loss_steps.append(loss) 
    if abs(loss - prev_loss) < epsilon:
        break
    prev_loss = loss
    i += 1
else:
    raise ValueError(f'Function does not convergance. Steps: {loss_steps}')
    
    
print(f'After {i} steps:\nt0: {counter.t0}\nt1: {counter.t1}\nloss: {loss}')


# In[11]:


import plotly.graph_objects as go


# In[12]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=df_rest['population'], y=df_rest['income'], name='real data', mode='markers'))
fig.add_trace(go.Scatter(x=df_rest['population'], y=counter.count_hipothesis(), name='hipothesis', mode='lines'))
fig.show()


# In[221]:


t0 = np.arange(-10, 0.1, 0.1)
t1 = np.arange(0, 3.1, 0.1)
z = np.empty([t1.size, ], dtype=object)

for i in range(t1.size):
    z[i] = [counter.set_coeff_and_count_loss(t0j, t1[i]) for t0j in t0]

    


# In[223]:


fig = go.Figure(data=[go.Surface(
    z=z, 
    x=t0, 
    y=t1, 
    colorscale=['indigo', 'blue', 'green', 'yellow', 'orange', 'red'],
)])
fig.update_layout(scene = dict(
                    xaxis_title='θ\u2080',
                    yaxis_title='θ\u2081',
                    zaxis_title='loss')
                 )


fig.show()


# In[224]:


fig = go.Figure(data=[go.Contour(
    z=z, 
    x=t0, 
    y=t1, 
    colorscale=['blue', 'green', 'yellow', 'orange', 'red'], 
    contours_coloring='lines', 
    colorbar={'title': 'loss'},
    zmax=50,
    zmin=0,
)])

fig['layout']['xaxis1'].update(title='θ\u2080')
fig['layout']['yaxis1'].update(title='θ\u2081')


fig.show()


# In[13]:


df_rental = pd.read_csv('ex1data2.txt', names=['space', 'rooms', 'cost'])


# In[146]:


class MultivariateCounter:
    def __init__(self, df: pd.DataFrame, initial_t: np.array=None):
        data = df.values.astype('float64')
        
        self.x = np.concatenate([
            np.full((data.shape[0], 1), 1), 
            data[:,:-1]
        ], axis=1)
        self.target = data[:,-1]
        
        self.t = initial_t if initial_t is not None else np.full(self.cardinality, 0)
        
    def scale(self):
        self.x[:, 1:] = self.x[:, 1:] - self.x[:, 1:].mean(axis=0)
        self.x[:, 1:] = self.x[:, 1:] / (self.x[:, 1:].max(axis=0) - self.x[:, 1:].min(axis=0))
        
    @property
    def size(self) -> int:
        return self.x.shape[0]
    
    @property
    def cardinality(self) -> int:
        # Number of input parameters + 1.
        return self.x.shape[1]
    
    def set_coeff_and_count_loss(self, t: np.array) -> float:
        self.set_coef(t)
        return self.count_loss()
        
    def set_coef(self,  t: np.array):
        self.t = t
    
    def count_hipothesis(self) -> np.array:
        return np.sum(self.x * self.t, axis=1)
    
    def count_loss(self):
        return np.mean((self.count_hipothesis() - self.target)**2)
    
    def count_derivative_j(self, j: int):
        return 2 / self.size * (
            np.sum((self.count_hipothesis() - self.target) * self.x[:,j])
        )    
    
    def make_gradient_descent(self, alpha: float) -> float:
        next_t = self._get_next_t(alpha)       
        return self.set_coeff_and_count_loss(next_t)
    
    def _get_next_t(self, alpha: float) -> np.array:
        return np.array([self.t[j] - self.count_derivative_j(j) * alpha for j in range(self.t.size)]) 
        
    
class VectorizedCounter(MultivariateCounter):
    def _get_next_t(self, alpha) -> np.array:
        return self.t - 2 * alpha / self.size * (
            np.sum(
                (self.count_hipothesis() - self.target).reshape((self.size, 1)) * self.x, 
                axis=0
            )
        )    


# In[147]:


class GradientDescent:
    def __init__(self, df, *, alpha=0.00000001, max_iter=1000, epsilon=100, counter_class=MultivariateCounter):
        self.counter = counter_class(df, np.array([0, 0, 0]))
        self.loss_steps = [self.counter.count_loss()]
        self.alpha = alpha
        self.max_iter = max_iter
        self.epsilon = epsilon
        
    def descent(self):
        i = 0
        while i < self.max_iter:
            loss = self.counter.make_gradient_descent(self.alpha)
            self.loss_steps.append(loss) 
            if abs(loss - self.loss_steps[-2]) < self.epsilon:
                break
            i += 1
        else:
            print(f'Function does not convergance.')


        print(f'After {i} steps:\nt: {self.counter.t}\nloss: {loss}')
    
    
class ScaleGD(GradientDescent):
    alpha = 0.1
    
    def __init__(self, df, *, alpha=0.1, **kwargs):
        super().__init__(df, alpha=alpha, **kwargs)
        self.counter.scale()
        
        
        
        


# In[148]:


d = GradientDescent(df_rental, epsilon=10)
d.descent()


# In[149]:


s = ScaleGD(df_rental, epsilon=10)
s.descent()


# In[150]:


all_scale = ScaleGD(df_rental, epsilon=10, counter_class=VectorizedCounter)
all_scale.descent()


# In[88]:


fig = go.Figure(
    data=[
        go.Scatter(x=np.arange(len(d.loss_steps)), y=d.loss_steps, name='no-scale'),
        go.Scatter(x=np.arange(len(s.loss_steps)), y=s.loss_steps, name='scale'),
    ],
    layout={
        'xaxis_title': 'steps',
        'yaxis_title': 'loss',
        'yaxis_type': 'log',
    }
)
fig.show()


# In[89]:


s_0001 = ScaleGD(df_rental, alpha=0.001)
s_0001.descent()

s_001 = ScaleGD(df_rental, alpha=0.01)
s_001.descent()

s_05 = ScaleGD(df_rental, alpha=0.5)
s_05.descent()

s_09 = ScaleGD(df_rental, alpha=0.9, epsilon=0)
s_09.descent()

s_1 = ScaleGD(df_rental, alpha=1, epsilon=0)
s_1.descent()


# In[90]:


fig = go.Figure(
    data=[
        go.Scatter(x=np.arange(len(s_1.loss_steps)), y=s_1.loss_steps, name='1'),
        go.Scatter(x=np.arange(len(s_0001.loss_steps)), y=s_0001.loss_steps, name='0.001'),
        go.Scatter(x=np.arange(len(s_001.loss_steps)), y=s_001.loss_steps, name='0.01'),
        go.Scatter(x=np.arange(len(s.loss_steps)), y=s.loss_steps, name='0.1'),
        go.Scatter(x=np.arange(len(s_05.loss_steps)), y=s_05.loss_steps, name='0.5'),
        go.Scatter(x=np.arange(len(s_09.loss_steps)), y=s_09.loss_steps, name='0.9'),        
    ],
    layout={
        'xaxis_title': 'steps',
        'yaxis_title': 'loss',
        'yaxis_type': 'log',
    }
)
fig.show()


# In[244]:


data = df_rental.values.astype('float64')
X = np.matrix(np.concatenate([np.full((data.shape[0], 1), 1), data[:,:-1]], axis=1))
y = np.matrix(data[:,-1].reshape(X.shape[0], 1))

calculated_theta = (X.transpose() * X)**(-1) * X.transpose() * y
counter = VectorizedCounter(df_rental, np.array(calculated_theta.transpose()))
counter.count_loss()


# In[ ]:




